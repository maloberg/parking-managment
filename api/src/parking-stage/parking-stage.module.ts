import { Module } from '@nestjs/common';
import { ParkingStageService } from './parking-stage.service';
import { ParkingStageController } from './parking-stage.controller';

@Module({
	controllers: [ ParkingStageController ],
	providers: [ ParkingStageService ]
})
export class ParkingStageModule { }
