import { Injectable } from '@nestjs/common';
import { PrismaService } from "src/prisma/prisma.service";
import { CreateParkingStageDto } from './dto/create-parking-stage.dto';
import { UpdateParkingStageDto } from './dto/update-parking-stage.dto';

@Injectable()
export class ParkingStageService {
	constructor(private prisma: PrismaService) { }

	create(createParkingStageDto: CreateParkingStageDto) {
		return this.prisma.parkingStage.create({ data: createParkingStageDto });
	}

	findAll() {
		return this.prisma.parkingStage.findMany({
			include: {
				parkingSpace: true

			}
		});
	}

	findOne(id: string) {
		return this.prisma.parkingStage.findUnique({ where: { id } });
	}

	update(id: string, updateParkingStageDto: UpdateParkingStageDto) {
		return this.prisma.parkingStage.update({
			where: { id },
			data: updateParkingStageDto
		});
	}

	remove(id: string) {
		return this.prisma.parkingStage.delete({ where: { id } });
	}
}
