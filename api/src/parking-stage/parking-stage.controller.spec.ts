import { Test, TestingModule } from '@nestjs/testing';
import { ParkingStageController } from './parking-stage.controller';
import { ParkingStageService } from './parking-stage.service';

describe('ParkingStageController', () => {
  let controller: ParkingStageController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ParkingStageController],
      providers: [ParkingStageService],
    }).compile();

    controller = module.get<ParkingStageController>(ParkingStageController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
