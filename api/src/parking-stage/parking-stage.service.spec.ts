import { Test, TestingModule } from '@nestjs/testing';
import { ParkingStageService } from './parking-stage.service';

describe('ParkingStageService', () => {
	let service: ParkingStageService;

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			providers: [ ParkingStageService ],
		}).compile();

		service = module.get<ParkingStageService>(ParkingStageService);
	});

	it('should be defined', () => {
		expect(service).toBeDefined();
	});
});
