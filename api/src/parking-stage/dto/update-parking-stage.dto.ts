import { PartialType } from '@nestjs/mapped-types';
import { CreateParkingStageDto } from './create-parking-stage.dto';

export class UpdateParkingStageDto extends PartialType(CreateParkingStageDto) {}
