import { Type } from "class-transformer";
import { IsArray, IsNumber, IsOptional, ValidateNested } from "class-validator";
import { CreateParkingSpaceDto } from "src/parking-space/dto/create-parking-space.dto";

export class CreateParkingStageDto {
	@IsNumber()
	stage: number;

	@IsOptional()
	@IsArray()
	@ValidateNested({ each: true })
	@Type(() => CreateParkingSpaceDto)
	parkingSpaces: CreateParkingSpaceDto[];
}
