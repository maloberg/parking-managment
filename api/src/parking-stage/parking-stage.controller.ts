import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';

import { ParkingStageService } from './parking-stage.service';
import { CreateParkingStageDto } from './dto/create-parking-stage.dto';
import { UpdateParkingStageDto } from './dto/update-parking-stage.dto';
import { Public } from "src/common/decorators";

@Controller('parking-stage')
export class ParkingStageController {
	constructor(private readonly parkingStageService: ParkingStageService) { }

	@Post()
	create(@Body() createParkingStageDto: CreateParkingStageDto) {
		return this.parkingStageService.create(createParkingStageDto);
	}

	@Public()
	@Get()
	findAll() {
		return this.parkingStageService.findAll();
	}

	@Public()
	@Get(':id')
	findOne(@Param('id') id: string) {
		return this.parkingStageService.findOne(id);
	}

	@Patch(':id')
	update(@Param('id') id: string, @Body() updateParkingStageDto: UpdateParkingStageDto) {
		return this.parkingStageService.update(id, updateParkingStageDto);
	}

	@Delete(':id')
	remove(@Param('id') id: string) {
		return this.parkingStageService.remove(id);
	}
}
