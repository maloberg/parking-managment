import { Injectable } from '@nestjs/common';
import { PrismaService } from "src/prisma/prisma.service";
import { CreateParkingSpaceDto } from './dto/create-parking-space.dto';
import { UpdateParkingSpaceDto } from './dto/update-parking-space.dto';

@Injectable()
export class ParkingSpaceService {
	constructor(private prisma: PrismaService) { }

	create(createParkingSpaceDto: CreateParkingSpaceDto) {
		return this.prisma.parkingSpace.create({
			data: createParkingSpaceDto
		});
	}

	findAll(stage?: number) {
		return this.prisma.parkingSpace.findMany();
	}

	findOne(id: string) {
		return this.prisma.parkingSpace.findUnique({
			where: {
				id
			}
		});
	}

	update(id: string, updateParkingSpaceDto: UpdateParkingSpaceDto) {
		//TODO: Check if user is already on a place and refute if this is the case 

		return this.prisma.parkingSpace.update({
			where: {
				id
			},
			data: updateParkingSpaceDto
		});
	}

	remove(id: string) {
		return this.prisma.parkingSpace.delete({
			where: {
				id
			}
		});
	}
}
