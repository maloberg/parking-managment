import { IsNumber, IsOptional, IsString } from "class-validator";

export class CreateParkingSpaceDto {
	@IsNumber()
	stage: number;

	@IsOptional()
	startingDate: Date;

	@IsString()
	parkingStageId: string;

	@IsOptional()
	@IsString()
	userId?: string;
}

