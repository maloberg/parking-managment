import { Controller, Get, Post, Body, Patch, Param, Delete, HttpCode, HttpStatus, UseGuards, Query } from '@nestjs/common';
import { ParkingSpaceService } from './parking-space.service';
import { CreateParkingSpaceDto } from './dto/create-parking-space.dto';
import { UpdateParkingSpaceDto } from './dto/update-parking-space.dto';
import { ParkingSpace } from "@prisma/client";
import { Public } from "src/common/decorators";
import { AdminGuard } from "src/common/guard/admin.guard";

@Controller('parking-space')
export class ParkingSpaceController {
	constructor(private readonly parkingSpaceService: ParkingSpaceService) { }

	@Post()
	@HttpCode(HttpStatus.CREATED)
	@UseGuards(AdminGuard)
	create(@Body() createParkingSpaceDto: CreateParkingSpaceDto): Promise<ParkingSpace> {
		return this.parkingSpaceService.create(createParkingSpaceDto);
	}

	@Public()
	@Get()
	@HttpCode(HttpStatus.OK)
	findAll(@Query("stage") stage: number): Promise<ParkingSpace[]> {
		return this.parkingSpaceService.findAll();
	}

	@Public()
	@Get(':id')
	@HttpCode(HttpStatus.OK)
	findOne(@Param('id') id: string): Promise<ParkingSpace> {
		return this.parkingSpaceService.findOne(id);
	}

	@Patch(':id')
	@HttpCode(HttpStatus.OK)
	update(@Param('id') id: string, @Body() updateParkingSpaceDto: UpdateParkingSpaceDto) {
		return this.parkingSpaceService.update(id, updateParkingSpaceDto);
	}

	@Delete(':id')
	@UseGuards(AdminGuard)
	@HttpCode(HttpStatus.OK)
	remove(@Param('id') id: string) {
		return this.parkingSpaceService.remove(id);
	}
}
