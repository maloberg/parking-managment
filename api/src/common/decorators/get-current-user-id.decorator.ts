import { createParamDecorator, ExecutionContext } from "@nestjs/common";

export const GetCurrentUserId = createParamDecorator(
    (context: ExecutionContext) => {
        console.log(context);
        const request = context.switchToHttp().getRequest();
        return request.user[ "sub" ];
    },
);
