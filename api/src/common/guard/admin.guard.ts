
import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Role } from "@prisma/client";
import { Observable } from 'rxjs';

@Injectable()
export class AdminGuard implements CanActivate {
	canActivate(
		context: ExecutionContext,
	): boolean | Promise<boolean> | Observable<boolean> {
		const user = context.switchToHttp().getRequest().user;

		if (!user) {
			return false;
		} else if (user.role !== Role.ADMIN) {
			return false;
		}

		return true;
	}
}