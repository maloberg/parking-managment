import {
    Body,
    Controller,
    HttpCode,
    HttpStatus,
    Post,
    Res,
    UseGuards,
} from "@nestjs/common";
import { AuthDto } from "./dto";
import { AuthService } from "./auth.service";
import { Tokens } from "./types";
import { RtGuard } from "src/common/guard";

import {
    GetCurrentUserId,
    Public,
    GetCurrentUser,
} from "src/common/decorators";

@Controller("auth")
export class AuthController {
    constructor(private authService: AuthService) { }

    @Public()
    @Post("local/signup")
    @HttpCode(HttpStatus.CREATED)
    signupLocal(@Body() dto: AuthDto): Promise<Tokens> {
        return this.authService.signupLocal(dto);
    }

    @Public()
    @Post("local/signin")
    @HttpCode(HttpStatus.OK)
    signinLocal(@Body() dto: AuthDto): Promise<Tokens> {
        return this.authService.signinLocal(dto);
    }

    @Post("logout")
    @HttpCode(HttpStatus.OK)
    logout(@GetCurrentUserId() id: string) {
        return this.authService.logout(id);
    }

    @Public()
    @Post("refresh")
    @UseGuards(RtGuard)
    @HttpCode(HttpStatus.OK)
    refreshToken(
        @GetCurrentUser("refreshToken") rt: string,
        @GetCurrentUser("sub") id: string,
    ): Promise<Tokens> {
        return this.authService.refresh(id, rt);
    }
}
