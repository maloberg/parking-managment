import { Role } from "@prisma/client";

export type Tokens = {
    access_token: string;
    refresh_token: string;
};

export type TokensPayload = {
    userId: string;
    email: string;
    role: Role;
};
