import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { APP_GUARD } from "@nestjs/core";
import { AuthModule } from "./auth/auth.module";
import { AtGuard } from "./common/guard";
// import { AtGuard } from "./common/guards";
import { PrismaModule } from "./prisma/prisma.module";
import { ParkingSpaceModule } from './parking-space/parking-space.module';
import { ParkingStageModule } from './parking-stage/parking-stage.module';

@Module({
    imports: [
        ConfigModule.forRoot({ isGlobal: true }),
        AuthModule,
        PrismaModule,
        ParkingSpaceModule,
        ParkingStageModule,
    ],
    providers: [
        {
            provide: APP_GUARD,
            useClass: AtGuard,
        },
    ],
})

export class AppModule { }
