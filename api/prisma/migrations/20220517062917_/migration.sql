/*
  Warnings:

  - You are about to drop the `User` table. If the table is not empty, all the data it contains will be lost.
  - A unique constraint covering the columns `[userId]` on the table `parking_space` will be added. If there are existing duplicate values, this will fail.

*/
-- DropIndex
DROP INDEX "parking_space_starting_date_idx";

-- AlterTable
ALTER TABLE "parking_space" ADD COLUMN     "userId" TEXT,
ALTER COLUMN "starting_date" SET DEFAULT CURRENT_TIMESTAMP;

-- DropTable
DROP TABLE "User";

-- CreateTable
CREATE TABLE "user" (
    "id" TEXT NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,
    "email" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "role" "Role" NOT NULL DEFAULT E'USER',
    "rt" TEXT,

    CONSTRAINT "user_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "user_email_key" ON "user"("email");

-- CreateIndex
CREATE UNIQUE INDEX "parking_space_userId_key" ON "parking_space"("userId");

-- CreateIndex
CREATE INDEX "parking_space_starting_date_userId_idx" ON "parking_space"("starting_date", "userId");

-- AddForeignKey
ALTER TABLE "parking_space" ADD CONSTRAINT "parking_space_userId_fkey" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE SET NULL ON UPDATE CASCADE;
