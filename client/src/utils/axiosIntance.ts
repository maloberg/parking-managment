import axios from "axios";

export const axiosInstance = axios.create({
	baseURL: "http://localhost:3001",
	headers: {
		'Content-Type': 'application/json',
		'Accept': 'application/json',
	}
});

const authorization = "authorization";

axiosInstance.interceptors.response.use(response => response, async (error) => {

	// TODO: Make a better way to handle connection

	const originalConfig = error.config;

	if (error?.response?.status === 401 && !originalConfig._retry) {
		const refreshToken = localStorage.getItem("rt");

		axiosInstance.defaults.headers.common[ authorization ] = `Bearer ${refreshToken}`;

		const res = await axiosInstance.post<Tokens>("/auth/refresh");

		localStorage.setItem("rt", res.data.refresh_token);

		originalConfig.headers[ "authorization" ] = `Bearer ${res.data.access_token}`;

		axiosInstance.defaults.headers.common[ authorization ] = `Bearer ${res.data.access_token}`;

		return Promise.resolve(axios(originalConfig));
	}

	return Promise.reject(error);
});

export type Tokens = {
	access_token: string;
	refresh_token: string;
};
