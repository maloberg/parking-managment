import { useMemo } from "react";
import { useQuery } from "react-query";

import { getParkingStages } from "services";
import ParkingStage from "components/ParkingStage/ParkingStage";

function Home() {
	const { data, error, isLoading } = useQuery("parking-space", getParkingStages);

	const filteredParkingSpace = useMemo(() => {
		if (!data) {
			return [];
		}

		//TODO: Best way is to do the filter from the backend with queryParams but i'm lazy maybe later.
		const numberOfStage = Array.from(new Set(data.map(element => (element.stage))));

		return numberOfStage.map(stage => (
			{
				id: stage,
				data: data.filter(space => (space.stage === stage))
			}
		));

	}, [ data ]);

	if (isLoading) {
		return <div style={ { paddingTop: "30%" } }>loading...</div>;
	}

	if (error) {
		return <div style={ { paddingTop: "30%" } }>error</div>;
	}

	return (
		<div style={ { paddingTop: "30%" } }>
			{ filteredParkingSpace.map(stage => (
				<ParkingStage key={ stage.id } stage={ stage.data } />
			)) }
		</div >
	);
}

export default Home;;