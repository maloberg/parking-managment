import { createContext, ReactNode, useContext, useEffect, useState } from "react";
import { axiosInstance, Tokens } from "utils/axiosIntance";

type IAuthContext = {
	login: ({ email, password }: Credentials) => void;
	register: ({ email, password }: Credentials) => void;
	logout: () => void;
};

type Credentials = {
	email: string;
	password: string;
};

const AuthContext = createContext({} as IAuthContext);

export const AuthContextProvider = ({ children }: { children: ReactNode; }) => {
	const login = async ({ email, password }: Credentials) => {
		try {
			const res = await axiosInstance.post<Tokens>("/auth/local/signin", {
				email,
				password
			});

			const tokens = res.data;

			axiosInstance.defaults.headers.common[ "authorization" ] = tokens.access_token;
			localStorage.setItem("rt", tokens.refresh_token);

		} catch (error) {
			console.error(error);
		}
	};

	const register = async ({ email, password }: Credentials) => {
		try {
			const res = await axiosInstance.post<Tokens>("/auth/local/signup", {
				email,
				password
			});

			const tokens = res.data;

			axiosInstance.defaults.headers.common[ "authorization" ] = tokens.access_token;
			localStorage.setItem("rt", tokens.refresh_token);

		} catch (error) {
			console.error(error);
		}
	};

	const logout = () => {
		axiosInstance.post("/logout");
	};

	const value = {
		logout,
		login,
		register
	};

	return (
		<AuthContext.Provider value={ value }>
			{ children }
		</AuthContext.Provider>
	);
};

export const useAuth = () => useContext(AuthContext);
