import { IParkingSpace } from "types/parkingSpace";
import { axiosInstance } from "utils/axiosIntance";

export const getParkingSpaces = async () => (
	(await axiosInstance.get<IParkingSpace[]>("/parking-space")).data
);

export const updateParkingSpaces = async ({ id, parkingSpace }: { id: string, parkingSpace: Partial<IParkingSpace>; }) => {
	return await axiosInstance.patch(`/parking-space/${id}`, parkingSpace);
};