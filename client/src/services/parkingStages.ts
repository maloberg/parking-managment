import { IParkingStage } from "types";
import { IParkingSpace } from "types/parkingSpace";
import { axiosInstance } from "utils/axiosIntance";

export const getParkingStages = async () => (
	(await axiosInstance.get<IParkingStage[]>("/parking-stage")).data
);

export const updateParkingStages = async ({ id, parkingSpace }: { id: string, parkingSpace: Partial<IParkingSpace>; }) => {
	return await axiosInstance.patch(`/parking-stage/${id}`, parkingSpace);
};