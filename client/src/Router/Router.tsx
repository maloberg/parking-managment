import { Route, Routes } from "react-router-dom";
import { lazy, Suspense } from "react";

const Home = lazy(() => import("pages/Home"));
const Layout = lazy(() => import("pages/Layout"));
const Page404 = lazy(() => import("pages/404"));

function Router() {
	return (
		<Suspense fallback={ <div>loading...</div> }>
			<Routes>
				<Route path="/" element={ <Layout /> }>
					<Route index element={ < Home /> } />
					<Route path="*" element={ <Page404 /> } />
				</Route>
			</Routes>
		</Suspense>
	);
}

export default Router;