import React from 'react';
import ReactDOM from 'react-dom/client';
import { QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";

import App from './App';

const client = new QueryClient({
	defaultOptions: {
		queries: {
			staleTime: 30000
		}
	}
});

ReactDOM.createRoot(document.getElementById('root')!).render(
	<React.StrictMode>
		<QueryClientProvider client={ client }>
			<App />
			{ import.meta.env.DEV ? <ReactQueryDevtools /> : null }
		</QueryClientProvider>
	</React.StrictMode>
);
