import { AuthContextProvider } from "context";
import { BrowserRouter } from "react-router-dom";
import Router from "Router/Router";

function App() {
	return (
		<BrowserRouter>
			<AuthContextProvider>
				<Router />
			</AuthContextProvider>
		</BrowserRouter>
	);
}

export default App;