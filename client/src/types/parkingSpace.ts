export type IParkingSpace = {
	id: string;
	userId: string;
	stage: number;
	number: number;
};