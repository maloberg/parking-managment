export type IParkingStage = {
	stage: number;
	parkingSpaces: IParkingStage[];
};