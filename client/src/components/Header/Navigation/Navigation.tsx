import { menu } from "components/Header/MenuList";
import { Link } from "react-router-dom";
import style from './Navigation.module.scss';
interface Props {
	open: boolean;
	toggleOpen: () => void;
}

const Navigation = ({ open, toggleOpen }: Props) => (
	<nav className={ `${style.navigation} ${open && style[ "navigation--open" ]}` }>
		<ul>
			{ menu.map(element => (
				<li key={ element.id }>
					<Link
						onClick={ toggleOpen }
						to={ element.path }
					>
						{ element.label }
					</Link>
				</li>
			)) }
		</ul>
	</nav>
);

export default Navigation;