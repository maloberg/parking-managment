export const menu = [
	{
		id: "1",
		label: "Bob",
		path: "/bob"
	},
	{
		id: "2",
		label: "Bob",
		path: "/fred"
	},
	{
		id: "3",
		label: "useQuery",
		path: "/query"
	},
	{
		id: "4",
		label: "context",
		path: "/context"
	},
];

export interface MenuItem {
	label: string,
	path: string;
}