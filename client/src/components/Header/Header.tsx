import Navigation from "./Navigation/Navigation";
import styles from './Header.module.scss';
import { useCallback, useState } from "react";
import { Link } from "react-router-dom";


function Header() {
	const [ open, setOpen ] = useState(false);
	const toggleOpen = useCallback(() => { setOpen(prev => !prev); }, []);

	return (
		<header className={ styles.menu }>
			<Link className={ styles.menu__logo } onClick={ () => setOpen(false) } to={ '/' }>
				<h2>Logo</h2>
			</Link>
			<Navigation toggleOpen={ toggleOpen } open={ open } />
			<div className={ styles.menu__toggle }>
				<button
					className={ `btn ${styles.menu__btn}` }
					onClick={ toggleOpen }
				>
					burger
				</button>
			</div>
		</header>
	);
}

export default Header;
