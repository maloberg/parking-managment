import { IParkingSpace } from "./ParkingSpace/ParikingSpace.type";
import ParkingSpace from "./ParkingSpace/ParkingSpace";

type Props = {
	stage: IParkingSpace[];
};

export default function ParkingStage({ stage }: Props) {
	return (
		<section>
			<ul>
				{ stage.map(space => (
					<li key={ space.id } >
						<ParkingSpace { ...space } />
					</li>
				)) }
			</ul>
		</section>
	);
}
