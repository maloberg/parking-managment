import { memo } from "react";
import { useMutation, useQueryClient } from "react-query";
import { updateParkingSpaces } from "services";
import { IParkingSpace } from "./ParikingSpace.type";

function ParkingSpace({ id, number, stage, userId }: IParkingSpace) {
	const queryClient = useQueryClient();

	const mutation = useMutation(updateParkingSpaces, {
		onSuccess: () => {
			queryClient.invalidateQueries("parking-space");
		}
	});

	const handleChange = () => {
		mutation.mutate({ id, parkingSpace: { stage } });
	};

	return (
		<>
			<div style={ { backgroundColor: userId ? "red" : "green" } }>parkingSpace</div>
		</>
	);
}

export default memo(ParkingSpace);
